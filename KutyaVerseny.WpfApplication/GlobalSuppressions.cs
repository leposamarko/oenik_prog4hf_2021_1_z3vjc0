﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Microsoft.Performance", "CA1812:AvoidUninstantiatedInternalClasses", Scope = "type", Target = "~T:KutyaVerseny.WpfApplication.UI.EditorServiceViaWindow", Justification = "<Silent>")]
[assembly: SuppressMessage("Performance", "CA1822:Mark members as static", Justification = "<Silent>", Scope = "member", Target = "~P:KutyaVerseny.WpfApplication.VM.EditorViewModel.TypeGenders")]
