var searchData=
[
  ['remov_285',['Remov',['../class_kutya_verseny_1_1_logic_1_1_doctor_logic.html#a9e94de0e1729fa36e65e5eb5b07ec0cf',1,'KutyaVerseny.Logic.DoctorLogic.Remov()'],['../interface_kutya_verseny_1_1_logic_1_1_i_doctor_logic.html#adce455978138175aca72f8f2bcb64976',1,'KutyaVerseny.Logic.IDoctorLogic.Remov()']]],
  ['remove_286',['Remove',['../interface_kutya_verseny_1_1_repository_1_1_i_repository.html#a20c598623682b2aaf03cfbe276cf9dd4',1,'KutyaVerseny.Repository.IRepository.Remove()'],['../class_kutya_verseny_1_1_repository_1_1_main_repository.html#ae9ab2714c1e78122768ec6cb5b48afa4',1,'KutyaVerseny.Repository.MainRepository.Remove()']]],
  ['removedog_287',['RemoveDog',['../interface_kutya_verseny_1_1_logic_1_1_i_owner_logic.html#a75374f6d602d613a743140594bc95eaa',1,'KutyaVerseny.Logic.IOwnerLogic.RemoveDog()'],['../class_kutya_verseny_1_1_logic_1_1_owner_logic.html#a7646400e4a228829261f01f7394f17f7',1,'KutyaVerseny.Logic.OwnerLogic.RemoveDog()']]],
  ['removedogbyid_288',['RemoveDogById',['../class_kutya_verseny_1_1_program_1_1_methods.html#ac306f2dbba5b88dc4966c50bdaaa10c0',1,'KutyaVerseny::Program::Methods']]],
  ['removeinterventionbyid_289',['RemoveInterventionById',['../class_kutya_verseny_1_1_program_1_1_methods.html#a2806778d3258ca2b23492e29a9995ad1',1,'KutyaVerseny::Program::Methods']]],
  ['removemedal_290',['RemoveMedal',['../class_kutya_verseny_1_1_logic_1_1_director_logic.html#ae65518ec0295bf7e057e0d4ac29484d9',1,'KutyaVerseny.Logic.DirectorLogic.RemoveMedal()'],['../interface_kutya_verseny_1_1_logic_1_1_i_director_logic.html#a43a669db19243941f47099a485dd8118',1,'KutyaVerseny.Logic.IDirectorLogic.RemoveMedal()']]],
  ['removemedalbyid_291',['RemoveMedalById',['../class_kutya_verseny_1_1_program_1_1_methods.html#ab98c282bf29c0464b06117094f823fec',1,'KutyaVerseny::Program::Methods']]],
  ['removemedaltest_292',['RemoveMedalTest',['../class_kutya_verseny_1_1_logic_1_1_tests_1_1_director_logic_tests.html#a0a77ff1a7d89b52f6e4f1a80694e53e1',1,'KutyaVerseny::Logic::Tests::DirectorLogicTests']]]
];
