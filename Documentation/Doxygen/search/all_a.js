var searchData=
[
  ['onconfiguring_127',['OnConfiguring',['../class_kutya_verseny_1_1_data_1_1_models_1_1_db.html#a6c37d2cc4fb1615c959f25e19d9fab8b',1,'KutyaVerseny::Data::Models::Db']]],
  ['oneintervetniontest_128',['OneIntervetnionTest',['../class_kutya_verseny_1_1_logic_1_1_tests_1_1_doctor_logic_tests.html#a6bd16dd057a07619443264b064ee0bf0',1,'KutyaVerseny::Logic::Tests::DoctorLogicTests']]],
  ['onmodelcreating_129',['OnModelCreating',['../class_kutya_verseny_1_1_data_1_1_models_1_1_db.html#ae2bef45f0d8aa6cab5ab3417f2b439b0',1,'KutyaVerseny::Data::Models::Db']]],
  ['ownerlogic_130',['OwnerLogic',['../class_kutya_verseny_1_1_logic_1_1_owner_logic.html',1,'KutyaVerseny.Logic.OwnerLogic'],['../class_kutya_verseny_1_1_logic_1_1_owner_logic.html#a8fcc15ffdfffc06ec86309f95660241d',1,'KutyaVerseny.Logic.OwnerLogic.OwnerLogic(IDogRepository repo, IInterventionRepositry intRepo, IMedalRepository medalRepo)'],['../class_kutya_verseny_1_1_logic_1_1_owner_logic.html#a2b88817c8724a99c5bf8224284365596',1,'KutyaVerseny.Logic.OwnerLogic.OwnerLogic(IDogRepository repo, IMedalRepository medalRepo)'],['../class_kutya_verseny_1_1_logic_1_1_owner_logic.html#a104a570e3d1f9d9c4fb0478e946adf87',1,'KutyaVerseny.Logic.OwnerLogic.OwnerLogic(IDogRepository repo)']]],
  ['ownerlogictests_131',['OwnerLogicTests',['../class_kutya_verseny_1_1_logic_1_1_tests_1_1_owner_logic_tests.html',1,'KutyaVerseny::Logic::Tests']]],
  ['ownermenu_132',['OwnerMenu',['../class_kutya_verseny_1_1_program_1_1_menu.html#ad7705bc642436a1fb883f359d1de175b',1,'KutyaVerseny::Program::Menu']]],
  ['ownername_133',['OwnerName',['../class_kutya_verseny_1_1_data_1_1_models_1_1_dog.html#a73e43c0efc57ee336d248df879881a0f',1,'KutyaVerseny::Data::Models::Dog']]]
];
