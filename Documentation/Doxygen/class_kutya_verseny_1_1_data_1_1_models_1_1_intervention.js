var class_kutya_verseny_1_1_data_1_1_models_1_1_intervention =
[
    [ "ToString", "class_kutya_verseny_1_1_data_1_1_models_1_1_intervention.html#a42cde348fb246f2161825a2b6da91aa7", null ],
    [ "Cost", "class_kutya_verseny_1_1_data_1_1_models_1_1_intervention.html#a59fcf2f155846484e8483dcac812f445", null ],
    [ "Desript", "class_kutya_verseny_1_1_data_1_1_models_1_1_intervention.html#a61d7c1d249e91d1bd74874a7d978a15d", null ],
    [ "Doctor", "class_kutya_verseny_1_1_data_1_1_models_1_1_intervention.html#a211ccf704d5b105402041e1efb735574", null ],
    [ "DoctorPhone", "class_kutya_verseny_1_1_data_1_1_models_1_1_intervention.html#a546eacb845d7d6928b0c04207bb9c69b", null ],
    [ "DogChipNum", "class_kutya_verseny_1_1_data_1_1_models_1_1_intervention.html#a7e3a7b8b95bb44d045067898aefdde5d", null ],
    [ "DogChipNumNavigation", "class_kutya_verseny_1_1_data_1_1_models_1_1_intervention.html#ad523636f95f59a1b533b66c2915921a2", null ],
    [ "InterventionDate", "class_kutya_verseny_1_1_data_1_1_models_1_1_intervention.html#a147ba143bd10f30f07c686f03bd4592e", null ],
    [ "InterventionId", "class_kutya_verseny_1_1_data_1_1_models_1_1_intervention.html#aa93d9b535a3f09882d869d25d881d7ec", null ]
];