var namespace_kutya_verseny_1_1_logic =
[
    [ "Tests", "namespace_kutya_verseny_1_1_logic_1_1_tests.html", "namespace_kutya_verseny_1_1_logic_1_1_tests" ],
    [ "DirectorLogic", "class_kutya_verseny_1_1_logic_1_1_director_logic.html", "class_kutya_verseny_1_1_logic_1_1_director_logic" ],
    [ "DoctorLogic", "class_kutya_verseny_1_1_logic_1_1_doctor_logic.html", "class_kutya_verseny_1_1_logic_1_1_doctor_logic" ],
    [ "IDirectorLogic", "interface_kutya_verseny_1_1_logic_1_1_i_director_logic.html", "interface_kutya_verseny_1_1_logic_1_1_i_director_logic" ],
    [ "IDoctorLogic", "interface_kutya_verseny_1_1_logic_1_1_i_doctor_logic.html", "interface_kutya_verseny_1_1_logic_1_1_i_doctor_logic" ],
    [ "IOwnerLogic", "interface_kutya_verseny_1_1_logic_1_1_i_owner_logic.html", "interface_kutya_verseny_1_1_logic_1_1_i_owner_logic" ],
    [ "OwnerLogic", "class_kutya_verseny_1_1_logic_1_1_owner_logic.html", "class_kutya_verseny_1_1_logic_1_1_owner_logic" ]
];