var interface_kutya_verseny_1_1_logic_1_1_i_owner_logic =
[
    [ "AddDog", "interface_kutya_verseny_1_1_logic_1_1_i_owner_logic.html#aff1d25035f223846be957fd0c3e1a83d", null ],
    [ "ChangeDogName", "interface_kutya_verseny_1_1_logic_1_1_i_owner_logic.html#a6929f3973a1727b3b2e6afe9a0857603", null ],
    [ "DogInterventionsAsync", "interface_kutya_verseny_1_1_logic_1_1_i_owner_logic.html#a287ff5395b58701dc89dc833056df50f", null ],
    [ "DogMedalsAsync", "interface_kutya_verseny_1_1_logic_1_1_i_owner_logic.html#ad707c28bc9cc6d33fa699ec88238e49e", null ],
    [ "DogsMedals", "interface_kutya_verseny_1_1_logic_1_1_i_owner_logic.html#a591d1108b521bdfdd98fac97b2d1d417", null ],
    [ "GetAllDogs", "interface_kutya_verseny_1_1_logic_1_1_i_owner_logic.html#a6d3f534a7a2b574f7e68b25c971eb9a0", null ],
    [ "GetAllIntervention", "interface_kutya_verseny_1_1_logic_1_1_i_owner_logic.html#a7e6599ae80d0390de2f6eed77d019f8c", null ],
    [ "GetAllMedal", "interface_kutya_verseny_1_1_logic_1_1_i_owner_logic.html#a980c4a2e9ed2a8f25c28dd0a302fb73d", null ],
    [ "GetYourDogByChip", "interface_kutya_verseny_1_1_logic_1_1_i_owner_logic.html#a3cdbd939371d1550226138345ef23de0", null ],
    [ "GetYourDogs", "interface_kutya_verseny_1_1_logic_1_1_i_owner_logic.html#a22622e9d774a6a1b97e3114dc16423df", null ],
    [ "RemoveDog", "interface_kutya_verseny_1_1_logic_1_1_i_owner_logic.html#a75374f6d602d613a743140594bc95eaa", null ]
];