var class_kutya_verseny_1_1_logic_1_1_owner_logic =
[
    [ "OwnerLogic", "class_kutya_verseny_1_1_logic_1_1_owner_logic.html#a8fcc15ffdfffc06ec86309f95660241d", null ],
    [ "OwnerLogic", "class_kutya_verseny_1_1_logic_1_1_owner_logic.html#a2b88817c8724a99c5bf8224284365596", null ],
    [ "OwnerLogic", "class_kutya_verseny_1_1_logic_1_1_owner_logic.html#a104a570e3d1f9d9c4fb0478e946adf87", null ],
    [ "AddDog", "class_kutya_verseny_1_1_logic_1_1_owner_logic.html#abc1b464e3fa5b51aa12ebd8478d7b1e9", null ],
    [ "AllOwner", "class_kutya_verseny_1_1_logic_1_1_owner_logic.html#ac11442c4c9dad2c6755e85899cb2f4d9", null ],
    [ "ChangeDogName", "class_kutya_verseny_1_1_logic_1_1_owner_logic.html#a235407121b6b3ebcdce9a84539a8606b", null ],
    [ "DogInterventionsAsync", "class_kutya_verseny_1_1_logic_1_1_owner_logic.html#a1cd0d4ccc1553d27d3da99487ba41ca6", null ],
    [ "DogMedalsAsync", "class_kutya_verseny_1_1_logic_1_1_owner_logic.html#a16ee20725912551c3ea43123687864eb", null ],
    [ "DogsInterventions", "class_kutya_verseny_1_1_logic_1_1_owner_logic.html#ad6b9a7f81ce507f133b50f4326c6525a", null ],
    [ "DogsMedals", "class_kutya_verseny_1_1_logic_1_1_owner_logic.html#a960f4951439ace171b0927a05b3fd7ec", null ],
    [ "GetAllDogs", "class_kutya_verseny_1_1_logic_1_1_owner_logic.html#ab4aaa821a5fbe7c5336d8964b20ec58b", null ],
    [ "GetAllIntervention", "class_kutya_verseny_1_1_logic_1_1_owner_logic.html#a252fba8d033e3f243a70f33d34c251f7", null ],
    [ "GetAllMedal", "class_kutya_verseny_1_1_logic_1_1_owner_logic.html#a0e2ce9138379f5a931d96bb86530da90", null ],
    [ "GetYourDogByChip", "class_kutya_verseny_1_1_logic_1_1_owner_logic.html#a06181cd0e7c6c9e1cbb88e04a679dca5", null ],
    [ "GetYourDogs", "class_kutya_verseny_1_1_logic_1_1_owner_logic.html#a24efb55ea723a12b61e9f026bf859a7b", null ],
    [ "RemoveDog", "class_kutya_verseny_1_1_logic_1_1_owner_logic.html#a7646400e4a228829261f01f7394f17f7", null ]
];