var namespace_kutya_verseny_1_1_repository =
[
    [ "DogRepository", "class_kutya_verseny_1_1_repository_1_1_dog_repository.html", "class_kutya_verseny_1_1_repository_1_1_dog_repository" ],
    [ "IDogRepository", "interface_kutya_verseny_1_1_repository_1_1_i_dog_repository.html", "interface_kutya_verseny_1_1_repository_1_1_i_dog_repository" ],
    [ "IInterventionRepositry", "interface_kutya_verseny_1_1_repository_1_1_i_intervention_repositry.html", "interface_kutya_verseny_1_1_repository_1_1_i_intervention_repositry" ],
    [ "IMedalRepository", "interface_kutya_verseny_1_1_repository_1_1_i_medal_repository.html", "interface_kutya_verseny_1_1_repository_1_1_i_medal_repository" ],
    [ "InterventionRepository", "class_kutya_verseny_1_1_repository_1_1_intervention_repository.html", "class_kutya_verseny_1_1_repository_1_1_intervention_repository" ],
    [ "IRepository", "interface_kutya_verseny_1_1_repository_1_1_i_repository.html", "interface_kutya_verseny_1_1_repository_1_1_i_repository" ],
    [ "MainRepository", "class_kutya_verseny_1_1_repository_1_1_main_repository.html", "class_kutya_verseny_1_1_repository_1_1_main_repository" ],
    [ "MedalRepository", "class_kutya_verseny_1_1_repository_1_1_medal_repository.html", "class_kutya_verseny_1_1_repository_1_1_medal_repository" ]
];