var class_kutya_verseny_1_1_data_1_1_models_1_1_dog =
[
    [ "Dog", "class_kutya_verseny_1_1_data_1_1_models_1_1_dog.html#a3f183bd4812a4d3ecb46095098773af7", null ],
    [ "ToString", "class_kutya_verseny_1_1_data_1_1_models_1_1_dog.html#a41dadb5786eb9b72764abe0df6cf6416", null ],
    [ "BornDate", "class_kutya_verseny_1_1_data_1_1_models_1_1_dog.html#ada401aa336ffb7a84b0cf575d0d1099c", null ],
    [ "Breed", "class_kutya_verseny_1_1_data_1_1_models_1_1_dog.html#ae9db38471d42ff306b0963cad2c48dea", null ],
    [ "ChipNum", "class_kutya_verseny_1_1_data_1_1_models_1_1_dog.html#aea5950f9f2893de631c2379d9b428cb6", null ],
    [ "DogName", "class_kutya_verseny_1_1_data_1_1_models_1_1_dog.html#aeeedf2560a5744bd8afc1773cc8a348f", null ],
    [ "Gender", "class_kutya_verseny_1_1_data_1_1_models_1_1_dog.html#a74fb934d58898de624287bffd5b82800", null ],
    [ "Intervention", "class_kutya_verseny_1_1_data_1_1_models_1_1_dog.html#ad820f71bf543f534c53cfdafceea97b7", null ],
    [ "Medal", "class_kutya_verseny_1_1_data_1_1_models_1_1_dog.html#ad90aa8374071e8d520a6ce90f973b16c", null ],
    [ "OwnerName", "class_kutya_verseny_1_1_data_1_1_models_1_1_dog.html#a73e43c0efc57ee336d248df879881a0f", null ]
];