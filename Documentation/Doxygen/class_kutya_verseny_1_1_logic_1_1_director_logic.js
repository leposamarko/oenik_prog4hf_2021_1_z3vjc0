var class_kutya_verseny_1_1_logic_1_1_director_logic =
[
    [ "DirectorLogic", "class_kutya_verseny_1_1_logic_1_1_director_logic.html#a7c01560912539a9121f40d20e4b7a947", null ],
    [ "DirectorLogic", "class_kutya_verseny_1_1_logic_1_1_director_logic.html#ac4aefa7fe81e7115346ec3ce102c8692", null ],
    [ "AddMedal", "class_kutya_verseny_1_1_logic_1_1_director_logic.html#ade171322d3d86aeb86452673087eefde", null ],
    [ "ChangeMedalCategory", "class_kutya_verseny_1_1_logic_1_1_director_logic.html#af21e4bd1c6822fca18a7ae204306d7f1", null ],
    [ "ChangeMedalDegree", "class_kutya_verseny_1_1_logic_1_1_director_logic.html#ae0c79a20c9376e97f5cb3db425b33fa9", null ],
    [ "ChangeMedalRaceName", "class_kutya_verseny_1_1_logic_1_1_director_logic.html#a624618550264726bf7227cbe199a359f", null ],
    [ "ChangeMedalStratersNum", "class_kutya_verseny_1_1_logic_1_1_director_logic.html#aaab7ada72267aa3e0d021675b6a1fbf7", null ],
    [ "DegreeNumb", "class_kutya_verseny_1_1_logic_1_1_director_logic.html#a50914429abab97c72c657f6e56469e42", null ],
    [ "DegreeNumbAsync", "class_kutya_verseny_1_1_logic_1_1_director_logic.html#abaf9e95afd93c561dc7559fc4b6484de", null ],
    [ "DogsWithThisDegree", "class_kutya_verseny_1_1_logic_1_1_director_logic.html#a025bdae7249544703c68816216b03ecd", null ],
    [ "GetAllMedal", "class_kutya_verseny_1_1_logic_1_1_director_logic.html#a01350e112080d51f4418d276e3350035", null ],
    [ "GetMedal", "class_kutya_verseny_1_1_logic_1_1_director_logic.html#a74512826d20b1f6d2a8d601b75ff7531", null ],
    [ "RemoveMedal", "class_kutya_verseny_1_1_logic_1_1_director_logic.html#ae65518ec0295bf7e057e0d4ac29484d9", null ]
];