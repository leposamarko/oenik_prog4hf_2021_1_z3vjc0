var class_kutya_verseny_1_1_logic_1_1_doctor_logic =
[
    [ "DoctorLogic", "class_kutya_verseny_1_1_logic_1_1_doctor_logic.html#a4c670fca1afbf0fb859b03212b2a0a03", null ],
    [ "DoctorLogic", "class_kutya_verseny_1_1_logic_1_1_doctor_logic.html#a1448bc3577df7de04debf9110df63f3b", null ],
    [ "Add", "class_kutya_verseny_1_1_logic_1_1_doctor_logic.html#a8d82f06e5fd01c83963f5549338bc470", null ],
    [ "AllDoctor", "class_kutya_verseny_1_1_logic_1_1_doctor_logic.html#a68e51972c2060d1eb338c33861e24a0a", null ],
    [ "AllInterventionForDoc", "class_kutya_verseny_1_1_logic_1_1_doctor_logic.html#ac4f85ceec692c9d8028bd9069df4c27f", null ],
    [ "ChangeInterventionCost", "class_kutya_verseny_1_1_logic_1_1_doctor_logic.html#a302ab92da15902a363462b6717ad7bda", null ],
    [ "ChangeInterventionDescript", "class_kutya_verseny_1_1_logic_1_1_doctor_logic.html#acddfa0969e48ce2e8b7822e42ba55615", null ],
    [ "ChangeInterventionDocPhone", "class_kutya_verseny_1_1_logic_1_1_doctor_logic.html#ad41e142bb2cbad66cabea3c0aba68be2", null ],
    [ "DogNeutering", "class_kutya_verseny_1_1_logic_1_1_doctor_logic.html#a2a586081b3b5920c8996f03899abef1a", null ],
    [ "GetAllIntervention", "class_kutya_verseny_1_1_logic_1_1_doctor_logic.html#ab7b6a249ab7c40f9886da707d64e41e9", null ],
    [ "GetIntervention", "class_kutya_verseny_1_1_logic_1_1_doctor_logic.html#a7af2e881b8386558f8d5132451d10e43", null ],
    [ "Remov", "class_kutya_verseny_1_1_logic_1_1_doctor_logic.html#a9e94de0e1729fa36e65e5eb5b07ec0cf", null ]
];