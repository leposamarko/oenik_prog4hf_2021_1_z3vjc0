var class_kutya_verseny_1_1_data_1_1_models_1_1_medal =
[
    [ "ToString", "class_kutya_verseny_1_1_data_1_1_models_1_1_medal.html#a0c54a4f894d6fe2c65cf3066d61d2e00", null ],
    [ "Category", "class_kutya_verseny_1_1_data_1_1_models_1_1_medal.html#ab297f5c0875f3a830ee0f329dff95667", null ],
    [ "Degree", "class_kutya_verseny_1_1_data_1_1_models_1_1_medal.html#a84b285214f936eaf88f887016f89d13d", null ],
    [ "DogChipNum", "class_kutya_verseny_1_1_data_1_1_models_1_1_medal.html#a4103ef4acb5b06b772095f323ba7a288", null ],
    [ "DogChipNumNavigation", "class_kutya_verseny_1_1_data_1_1_models_1_1_medal.html#a30023d49b269ccfb83e96cc85e5e439f", null ],
    [ "MedalId", "class_kutya_verseny_1_1_data_1_1_models_1_1_medal.html#a92e5c2703372d330cef08e1b1f637b7a", null ],
    [ "RaceName", "class_kutya_verseny_1_1_data_1_1_models_1_1_medal.html#aa989754960a5a62b73544aa52f0ea2cc", null ],
    [ "StartersNum", "class_kutya_verseny_1_1_data_1_1_models_1_1_medal.html#ada938dfea7bd864cb4d55e5feb6a44f9", null ],
    [ "WonDate", "class_kutya_verseny_1_1_data_1_1_models_1_1_medal.html#a23dd9a590bdba762e8882cdf769cf7dd", null ]
];