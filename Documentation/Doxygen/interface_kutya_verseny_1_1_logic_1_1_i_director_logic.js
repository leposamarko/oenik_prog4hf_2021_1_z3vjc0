var interface_kutya_verseny_1_1_logic_1_1_i_director_logic =
[
    [ "AddMedal", "interface_kutya_verseny_1_1_logic_1_1_i_director_logic.html#ae2563518e5e81133922249b1f555dc0b", null ],
    [ "ChangeMedalCategory", "interface_kutya_verseny_1_1_logic_1_1_i_director_logic.html#a707ce1462903b39f0fb86fc314ff95e1", null ],
    [ "ChangeMedalDegree", "interface_kutya_verseny_1_1_logic_1_1_i_director_logic.html#a8c7a9c4e7560faafcc86b54054279ea4", null ],
    [ "ChangeMedalRaceName", "interface_kutya_verseny_1_1_logic_1_1_i_director_logic.html#a44a417ba871cf6d5e85d0393662fb5c4", null ],
    [ "ChangeMedalStratersNum", "interface_kutya_verseny_1_1_logic_1_1_i_director_logic.html#a4c95528845ef1187b2d5ebdedc2015f9", null ],
    [ "DegreeNumb", "interface_kutya_verseny_1_1_logic_1_1_i_director_logic.html#aafcffcfedb744f474e74236387ca82e8", null ],
    [ "DegreeNumbAsync", "interface_kutya_verseny_1_1_logic_1_1_i_director_logic.html#a789f20b907e0b8779076e8301beefaf1", null ],
    [ "DogsWithThisDegree", "interface_kutya_verseny_1_1_logic_1_1_i_director_logic.html#ae7a9e43627d528030908b6a0dfb9807d", null ],
    [ "GetAllMedal", "interface_kutya_verseny_1_1_logic_1_1_i_director_logic.html#ab23193aaf949d11e732fd84c3d8d6899", null ],
    [ "GetMedal", "interface_kutya_verseny_1_1_logic_1_1_i_director_logic.html#aa54f5814e7842858dceb4967d2773b9e", null ],
    [ "RemoveMedal", "interface_kutya_verseny_1_1_logic_1_1_i_director_logic.html#a43a669db19243941f47099a485dd8118", null ]
];